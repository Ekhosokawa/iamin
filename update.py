import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'iamin_project.settings')

import django
django.setup()
from datetime import datetime
import pytz
import random
from django.utils import timezone
from iamin.models import Degree, Student, Course, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence, ModifiedUser
from django.contrib.auth.hashers import make_password
from django.conf import settings

#Python conventions for methods use underscore (snake_case) over camelCase.
#global variable to get the file directory for population .txt files
text_dir = settings.TEXTFILES_DIR
#set here so that it's easy to locate/edit
#Order should always be: degrees, students, instructors, courses, lectures, lecture_occurrences
# population_files = ["degrees", "students", "instructors", "courses", "lectures", "lecture_occurrences"]
update_files = ["ENGLANG_0014_Tue", "COMPLIT_1392_Wed", "ENGLANG_0014_Feb18", "ENGLANG_0014_Feb25"]

def update_current():
    for file_name in update_files:
        file_path = text_dir + "/" + file_name + ".txt"
        # check to see if file exists
        if (not os.path.isfile(file_path)):
            print("Does not exist: " + file_path)
        else:
            f = open(file_path, 'r')
            print (file_path + " opened")
            # Python try/catch
            with open (file_path) as f:
                class_info = f.readline()
                line0=class_info.rstrip()
                split_info = line0.split(", ")
                for line in f:
                    line=line.rstrip()
                    split_string = line.split(", ")
                    print("hereee")
                    print(split_string)
                    print(split_info)
                    add_evaluation_data(split_info, split_string)
            print ("file closed:" + str(f.closed))
    #for the student's evaluation tasks:
    date = "2020-03-05"
    course_id = "COMPLIT 4826"
    day = "Thu"
    course = Course.objects.get(id_no=course_id)
    lecture = Lecture.objects.get(course=course, day=day)
    #need to use date/time objects, not a date/time-as-string
    time = lecture.start_time
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
    instr_mod = ModifiedUser.objects.get(username = "59264027")
    instr = Instructor.objects.get(user=instr_mod)
    occurrence = LectureOccurrence.objects.get_or_create(regular_lecture = lecture, date=datetime_object, instructor = instr)[0]
    occurrence.session_password = "neverLetMeGo"
    occurrence.save()

    #student data, March 12
    date = "2020-03-12"
    course_id = "COMPLIT 4826"
    day = "Thu"
    course = Course.objects.get(id_no=course_id)
    lecture = Lecture.objects.get(course=course, day=day)
    #need to use date/time objects, not a date/time-as-string
    time = lecture.start_time
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
    instr_mod = ModifiedUser.objects.get(username = "59264027")
    instr = Instructor.objects.get(user=instr_mod)
    occurrence = LectureOccurrence.objects.get_or_create(regular_lecture = lecture, date=datetime_object, instructor = instr)[0]
    occurrence.save()


    print(occurrence.date, occurrence.session_password)
    #creating a notification for the evaluation of the student version
    notif_course = Course.objects.get(id_no="CRWRT 0309")
    notif_lec = Lecture.objects.get(course=notif_course, day = "Tue")
    notif_date = "2020-03-03"
    notif_time = notif_lec.start_time
    notif_date_obj = datetime.strptime(notif_date, '%Y-%m-%d')
    notif_date_obj = notif_date_obj.replace(hour=notif_time.hour, minute=notif_time.minute, tzinfo=pytz.UTC)
    notif_occ = LectureOccurrence.objects.get_or_create(regular_lecture=notif_lec, date=notif_date_obj)[0]
    if (notif_occ.message == None):
        notif_occ.message = "Guest lecture by Junot Diaz"
        notif_occ.save()

    print(notif_occ.message)



def add_evaluation_data(split_info, split_string):
    #assigning array values to variables
    course_id = split_info[0]
    day = split_info[1]
    start_time = split_info[2]
    location = split_info[3]
    date = split_string[0]
    password = split_string[1]
    unsplit_absent = split_string[2]
    unsplit_excused = split_string[3]
    unsplit_present = split_string[4]
    isActiveBoolean = split_string[5]
    #Check if any of the values are empty:
    if (unsplit_absent=="[]"):
        absent_students = "" #set empty array if value is empty
    else:
        absent_students = str(unsplit_absent).strip("[]").split("; ")
        print("ABSENT")
        print(absent_students)
    if (unsplit_excused=="[]"):
        excused_students=""
    else:
        excused_students = str(unsplit_excused).strip("[]").split("; ")
        print("EXCUSED")
        print(excused_students)
    if (unsplit_present=="[]"):
        present_students=""
    else:
        present_students = str(unsplit_present).strip("[]").split("; ")
        print("PRESENT")
        print(present_students)

    course = Course.objects.get(id_no=course_id)
    lecture = Lecture.objects.get(course=course, day=day, start_time=start_time, location=location)
    #need to use date/time objects, not a date/time-as-string
    time = lecture.start_time
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)

    #for remaking LectureOccurences after clearing the database for evaluation
    lecocc_query = LectureOccurrence.objects.filter(regular_lecture = lecture, date=datetime_object)
    if (lecocc_query.count() == 0):
        #If the remade lecture objects are from the 18th and 25th, remake  with the original instructor
        if (date == "2020-02-18" or date == "2020-02-25"):
            mod_user = ModifiedUser.objects.get(username = "36926702")
        else:
            mod_user = ModifiedUser.objects.get(username = "12345678")
        instr = Instructor.objects.get(user = mod_user)
        occurrence = LectureOccurrence.objects.get_or_create(regular_lecture = lecture, date=datetime_object, instructor=instr)[0]
    else:
        occurrence = LectureOccurrence.objects.get(regular_lecture = lecture, date=datetime_object)
    # occurrence = LectureOccurrence.objects.get_or_create(regular_lecture=lecture, date=datetime_object)[0]
    # if (occurrence.session_password == None):
    #     occurrence.session_password=password
    # else:
    #     print (occurrence.session_password)

    # Cleaning up lists in case population script is being re-ran but the database wasn't deleted:
    occurrence.session_password = password
    occurrence.absent_list.clear()
    occurrence.attendance_list.clear()
    occurrence.excused_list.clear()
    for each in absent_students:
        user = ModifiedUser.objects.get(username=each)
        student = Student.objects.get(user=user)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        #Bad excuse generator for fun (students are marked as absent regardless of no excuse or rejected excuse)
        has_excuse = random.choice([True,False])
        if (has_excuse):
            slo.excuse=excuse_generator("any")
            slo.attended=3
            occurrence.pending_list.add(slo.student)
        else:
            #Cleaning up again
            slo.excuse=None
            slo.attended=0
            occurrence.absent_list.add(slo.student)
        slo.save()
    for each in excused_students:
        user = ModifiedUser.objects.get(username=each)
        student = Student.objects.get(user=user)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        slo.excuse=excuse_generator("good")
        slo.attended=2
        occurrence.excused_list.add(slo.student)
        slo.save()
    if (occurrence.session_password != ""):
        for each in present_students:
            user = ModifiedUser.objects.get(username=each)
            student = Student.objects.get(user=user)
            slo = StudentLectureOccurrence.objects.get_or_create(
                lecture_occurrence=occurrence, student=student)[0]
            slo.session_password=password
            slo.attended=1
            occurrence.attendance_list.add(slo.student)
            slo.save()
    else:
        occurrence.session_password = None

    #For the absent student in the instructor's evaluation tasks:
    if (date == "2020-02-18" or date == "2020-02-25"):
        user = ModifiedUser.objects.get(username = "34255578")
        student = Student.objects.get(user=user)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        slo.attended=3
        slo.excuse = "I've had a medical emergency"
        occurrence.pending_list.add(slo.student)
        slo.save()

    print(isActiveBoolean)
    occurrence.is_active=isActiveBoolean
    print(occurrence.is_active)
    print(occurrence.session_password)
    occurrence.save()

def excuse_generator(type):
    good_excuses=["I'm sick","doctor's appointment","dentist appointment","out of town","don't feel well","job interview","my pet is sick","my roommate is sick","my family member is sick","my car broke down"]
    bad_excuses=["forgot about class","my plant is sick","I had to organize my dog's birthday party","it's raining","my girlfriend's goldfish died","it's my friend's brother's boyfriend's birthday"]
    all_excuses=good_excuses+bad_excuses
    if (type=="good"):
        myNumber=random.randint(0,len(good_excuses)-1) #-1 so index isn't out of bounds
        excuse = good_excuses[myNumber]
    elif(type=="bad"):
        myNumber=random.randint(0,len(bad_excuses)-1)
        excuse = bad_excuses[myNumber]
    elif(type=="any"):
        myNumber=random.randint(0,len(all_excuses)-1)
        excuse = all_excuses[myNumber]
    return(excuse)

def clear_current(course0, day0, date0):
    # print("You are in clear_current view from update.py")
    cou = Course.objects.get(id_no = course0)
    lec = Lecture.objects.get(course = cou, day = day0)
    time = lec.start_time
    date = date0
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
    lecocc = LectureOccurrence.objects.get(regular_lecture = lec, date=datetime_object)
    lecocc.delete()

def clear_SLO(user):
    student = Student.objects.get(user=user)
    course_id = "COMPLIT 4826"
    day = "Thu"
    course = Course.objects.get(id_no=course_id)
    lecture = Lecture.objects.get(course=course, day=day)
    time = lecture.start_time
    date = ["2020-03-05", "2020-03-12"]
    for each in date:
        datetime_object = datetime.strptime(each, '%Y-%m-%d')
        datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
        occurrence = LectureOccurrence.objects.filter(regular_lecture = lecture, date=datetime_object)
        if (occurrence.count() != 0):
            print ("occurrence: ", occurrence)
            for occ in occurrence:
                print("occ: ", occ)
                slo_query = StudentLectureOccurrence.objects.filter(lecture_occurrence=occ, student=student)
                if (slo_query.count() != 0):
                    for slo in slo_query:
                        slo.delete()
                occ.delete()
    # Get all lecture occurrences on whatever dates
    # filter all SLO where student = student, occ = occ
    # delete all of them
    print("end of clear_slo method")

def hello_world():
    print("Hey, you successfully imported update.py")

if __name__ == '__main__':
    update_current()
    # clear_current("ENGLANG 0012", "Mon", "2020-02-10")
