import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'iamin_project.settings')

import django
django.setup()
from datetime import datetime
import pytz
import random
from django.utils import timezone
from iamin.models import Degree, Student, Course, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence, ModifiedUser
from django.contrib.auth.hashers import make_password
from django.conf import settings

#Python conventions for methods use underscore (snake_case) over camelCase.
#global variable to get the file directory for population .txt files
text_dir = settings.TEXTFILES_DIR
#set here so that it's easy to locate/edit
#Order should always be: degrees, students, instructors, courses, lectures, lecture_occurrences
population_files = ["degrees", "students", "instructors", "courses", "lectures", "lecture_occurrences"]

def populate():
    for file_name in population_files:
        file_path = text_dir + "/" + file_name + ".txt"
        # check to see if file exists
        if (not os.path.isfile(file_path)):
            print("Does not exist: " + file_path)
        else:
            f = open(file_path, 'r')
            print (file_path + " opened")
            # Python try/catch
            with open (file_path) as f:
                for line in f:
                    #Clean whitespace
                    line=line.rstrip()
                    split_string = line.split(", ")
                    if (file_name=="degrees"):
                        add_degrees(split_string)
                    elif (file_name=="students"):
                        add_students(split_string)
                    elif (file_name=="instructors"):
                        add_instructors(split_string)
                    elif (file_name=="courses"):
                        add_courses(split_string)
                    elif (file_name=="lectures"):
                        add_lectures(split_string)
                    elif (file_name=="lecture_occurrences"):
                        add_lecture_occurrences(split_string)
            print (file_name + " closed:" + str(f.closed))

def add_degrees(split_string):
    name = split_string[0]
    type = split_string[1]
    school = split_string[2]
    abrv = split_string[3]
    d = Degree.objects.get_or_create(name=name)[0]
    d.type=type
    d.school=school
    d.abrv=abrv
    d.save()

def add_students(split_string):
# def add_stu_test(username, password, lastName, firstNames, email, degree):
    username = split_string[0]
    password = split_string[1]
    lastName = split_string[2]
    firstNames = split_string[3]
    email = split_string[4]
    degree = split_string[5]
    degree0=Degree.objects.get(name=degree)
    mypassword= make_password(password, salt=None, hasher='default')
    user0=ModifiedUser.objects.get_or_create(username=username, password=mypassword,
        first_name=firstNames, last_name=lastName, email=email, user_type=1)[0]
    s = Student.objects.get_or_create(user=user0, degree=degree0)[0]
    # s.save()
    # return s

def add_instructors(split_string):
# def add_stu_test(username, password, lastName, firstNames, email, degree):
    username = split_string[0]
    password = split_string[1]
    lastName = split_string[2]
    firstNames = split_string[3]
    email = split_string[4]
    mypassword= make_password(password, salt=None, hasher='default')
    user0=ModifiedUser.objects.get_or_create(username=username, password=mypassword,
        first_name=firstNames, last_name=lastName, email=email, user_type=2)[0]
    i = Instructor.objects.get_or_create(user=user0)[0]
    i.save()
    return i

def add_courses(split_string):
# def add_cou(name, id_no, instructor, degree, students):
    name = split_string[0]
    id_no = split_string[1]
    unsplit_instructors = split_string[2]
    #cast instructors to string, strip brackets, split between ; make array
    instructors = str(unsplit_instructors).strip("[]").split("; ")
    unsplit_degrees = split_string[3]
    degrees = str(unsplit_degrees).strip("[]").split("; ")
    unsplit_students = split_string[4]
    students = str(unsplit_students).strip("[]").split("; ")
    degree0 = [] #empty array
    instructor0 = []
    student0 = []
    for ins in instructors: #iterator for a course with multiple instructors
        #get the user object where the username is the same as the instructor's username
        user0 = ModifiedUser.objects.get(username=ins)
        instructor0.append(Instructor.objects.get(user=user0))
    for deg in degrees: #same as above
        degree0.append(Degree.objects.get(name=deg))
    for stu in students: #same as above
        user0 = ModifiedUser.objects.get(username=stu)
        student0.append(Student.objects.get(user=user0))
    c = Course.objects.get_or_create(id_no=id_no)[0]
    #Because degree is a ManyToManyField, the .set method must be called instead
    c.degree.set(degree0)
    c.instructor.set(instructor0)
    c.students.set(student0)
    c.name=name
    c.save()
    return c

def add_lectures(split_string):
    course_id = split_string[0]
    day = split_string[1]
    start_time = split_string[2]
    end_time = split_string[3]
    location = split_string[4]
    course0=Course.objects.get(id_no=course_id)
    l = Lecture.objects.get_or_create(course=course0, day=day, start_time=start_time, location=location)[0]
    l.end_time=end_time
    l.save()
    # print(l)

def add_lecture_occurrences(split_string):
    course_id = split_string[0]
    day = split_string[1]
    start_time = split_string[2]
    location = split_string[3]
    date = split_string[4]
    instructor = split_string[5]
    course0 = Course.objects.get(id_no=course_id)
    # print(course0)
    lecture0 = Lecture.objects.get(course=course0, day=day, start_time=start_time, location=location)
    # print(lecture0)
    #returns python Time object from the lecture object
    time = lecture0.start_time
    #create datetime object
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    #change time attributes in datetime_object with lecture time; set timezone info
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
    user0 = ModifiedUser.objects.get(username=instructor)
    instructor0=Instructor.objects.get(user=user0)
    o = LectureOccurrence.objects.get_or_create(regular_lecture=lecture0, date=datetime_object, instructor=instructor0)[0]
    # print(o)

if __name__ == '__main__':
    populate()
