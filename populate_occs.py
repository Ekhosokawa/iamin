import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'iamin_project.settings')

import django
django.setup()
from datetime import datetime
import pytz
import random
from django.utils import timezone
from iamin.models import Degree, Student, Course, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence, ModifiedUser
from django.contrib.auth.hashers import make_password
from django.conf import settings

#Python conventions for methods use underscore (snake_case) over camelCase.
#global variable to get the file directory for population .txt files
text_dir = settings.TEXTFILES_DIR
#set here so that it's easy to locate/edit
#Order should always be: degrees, students, instructors, courses, lectures, lecture_occurrences
# population_files = ["degrees", "students", "instructors", "courses", "lectures", "lecture_occurrences"]
update_past_files = ["past_lectures_01", "past_lectures_02", "past_lectures_03", "past_lectures_04", "past_lectures_05",
    "past_lectures_06", "past_lectures_07", "past_lectures_08", "past_lectures_09", "past_lectures_10", "past_lectures_11",
    "past_lectures_12", "past_lectures_13", "past_lectures_14", "past_lectures_15", "ENGLANG_0012_Mon"]

def update_past():
    for file_name in update_past_files:
        file_path = text_dir + "/" + file_name + ".txt"
        # check to see if file exists
        if (not os.path.isfile(file_path)):
            print("Does not exist: " + file_path)
        else:
            f = open(file_path, 'r')
            print (file_path + " opened")
            # Python try/catch
            with open (file_path) as f:
                class_info = f.readline()
                line0=class_info.rstrip()
                split_info = line0.split(", ")
                for line in f:
                    line=line.rstrip()
                    split_string = line.split(", ")
                    add_old_lectures(split_info, split_string)
            print ("file closed:" + str(f.closed))

def add_old_lectures(split_info, split_string):
    #assigning array values to variables
    course_id = split_info[0]
    day = split_info[1]
    start_time = split_info[2]
    location = split_info[3]
    date = split_string[0]
    password = split_string[1]
    unsplit_absent = split_string[2]
    #Check if any of the values are empty:
    if (unsplit_absent=="[]"):
        absent_students = "" #set empty array if value is empty
    else:
        absent_students = str(unsplit_absent).strip("[]").split("; ")
    unsplit_excused = split_string[3]
    if (unsplit_excused=="[]"):
        excused_students=""
    else:
        excused_students = str(unsplit_excused).strip("[]").split("; ")
    unsplit_present = split_string[4]
    if (unsplit_present=="[]"):
        present_students=""
    else:
        present_students = str(unsplit_present).strip("[]").split("; ")
    course = Course.objects.get(id_no=course_id)
    lecture = Lecture.objects.get(course=course, day=day, start_time=start_time, location=location)
    #need to use date/time objects, not a date/time-as-string
    time = lecture.start_time
    datetime_object = datetime.strptime(date, '%Y-%m-%d')
    datetime_object = datetime_object.replace(hour=time.hour, minute=time.minute, tzinfo=pytz.UTC)
    occurrence = LectureOccurrence.objects.get_or_create(regular_lecture=lecture, date=datetime_object)[0]
    occurrence.session_password=password
    # Cleaning up lists in case population script is being re-ran but the database wasn't deleted:
    occurrence.absent_list.clear()
    occurrence.attendance_list.clear()
    occurrence.excused_list.clear()
    for each in absent_students:
        user = ModifiedUser.objects.get(username=each)
        student = Student.objects.get(user=user)
        # print(student.user.first_name, student.user.last_name)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        #Bad excuse generator for fun (students are marked as absent regardless of no excuse or rejected excuse)
        has_bad_excuse = random.choice([True,False])
        if (has_bad_excuse):
            slo.excuse=excuse_generator("bad")
        else:
            #Cleaning up again
            slo.excuse=None
        # print(slo.excuse)
        slo.attended=0
        occurrence.absent_list.add(slo.student)
        slo.save()
    for each in excused_students:
        user = ModifiedUser.objects.get(username=each)
        student = Student.objects.get(user=user)
        # print(student.user.first_name, student.user.last_name)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        slo.excuse=excuse_generator("good")
        # print(slo.excuse)
        slo.attended=2
        occurrence.excused_list.add(slo.student)
        slo.save()
    for each in present_students:
        user = ModifiedUser.objects.get(username=each)
        student = Student.objects.get(user=user)
        # print(student.user.first_name, student.user.last_name)
        slo = StudentLectureOccurrence.objects.get_or_create(
            lecture_occurrence=occurrence, student=student)[0]
        slo.session_password=password
        slo.attended=1
        occurrence.attendance_list.add(slo.student)
        slo.save()
    occurrence.is_active=False
    occurrence.save()

def excuse_generator(type):
    good_excuses=["I'm sick","doctor's appointment","dentist appointment","out of town","don't feel well","job interview","my pet is sick","my roommate is sick","my family member is sick","my car broke down"]
    bad_excuses=["forgot about class","my plant is sick","I had to organize my dog's birthday party","it's raining","my girlfriend's goldfish died","it's my friend's brother's boyfriend's birthday"]
    all_excuses=good_excuses+bad_excuses
    if (type=="good"):
        myNumber=random.randint(0,len(good_excuses)-1) #-1 so index isn't out of bounds
        excuse = good_excuses[myNumber]
    elif(type=="bad"):
        myNumber=random.randint(0,len(bad_excuses)-1)
        excuse = bad_excuses[myNumber]
    elif(type=="any"):
        myNumber=random.randint(0,len(all_excuses)-1)
        excuse = any_excuses[myNumber]
    return(excuse)

if __name__ == '__main__':
    update_past()
