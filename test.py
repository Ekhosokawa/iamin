import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'iamin_project.settings')
import django
import random
import re
import itertools
django.setup()
import datetime
from django.conf import settings
from iamin.models import Degree, Student, Course, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence, ModifiedUser
from django.db.models import Count, Avg

text_dir = settings.TEXTFILES_DIR
all_students = []
all_instr = []

def file_reader():
    all_files = {"past_lectures_01",}
    for each in all_files:
        file_path = text_dir + "\\" + each + ".txt"
        print (file_path)
        f = open(file_path, 'r')
        with open (file_path) as f:
            class_info = f.readline()
            print(class_info)
            for line in f:
                line=line.rstrip()
                split_string = line.split(", ")
                # print(split_string)
                print(split_string)
                # print(class_info)
        print ("file closed:" + str(f.closed))
        # print(degrees)
    # print(f.closed)

def randomizer():
    # print(randrange(10))
    # for x in range(15):
    #     print (random.randint(1,2))
    has_bad_excuse = random.choice([True,False])
    if (has_bad_excuse):
        print("true")
    else:
        print("false")

def printer():
    mlittstring = ""
    degstr = "Comparative Literature (MRes)"
    deg = Degree.objects.get(name = degstr)
    # print(deg)
    query_mlitt = Student.objects.filter(degree = deg)
    for each in query_mlitt:
        id = each.user.username
        mlittstring += id + "; "

    print("All MRes students: ")
    print(mlittstring)

def absent_excused_generator():
    #Modify these as needed
    course_id = "ENGLANG 0014"
    # day = "Mon"
    # time = "15:00"
    # location = "Graham Kerr 231"
    course = Course.objects.get(id_no = course_id)
    lecture_query = Lecture.objects.get(course=course)
    occ_query=LectureOccurrence.objects.filter(regular_lecture=lecture_query)
    dates=[]
    for each in occ_query:
        dates.append(each.date)
    print(dates[0], dates[1], dates[2], dates[3])
    print(course.instructor.all(), course.name)
    date = "2020-01-17"
    password = "theEmpathyExams"
    generateNo = 9

    print("total count: ", course.students.count())


    # Comment starting from here, change generateNo as needed
    absentNo = random.randint(0,generateNo)
    excusedNo = generateNo-absentNo



    all_students_query = course.students.all()
    all_students = []
    for each in all_students_query:
        all_students.append(each.user.username)
    absent_sampling = random.choices(all_students, k=absentNo)
    for each in absent_sampling:
        all_students.remove(each)
    excused_sampling = random.choices(all_students, k=excusedNo)
    for each in excused_sampling:
        all_students.remove(each)
    absent_sample_string = str(absent_sampling)
    absent_replaced = re.sub('[,]', ';', absent_sample_string)
    absent_replaced = re.sub("[']", '', absent_replaced)
    excused_sample_string = str(excused_sampling)
    excused_replaced = re.sub('[,]', ';', excused_sample_string)
    excused_replaced = re.sub("[']", '', excused_replaced)
    present_sample_string = str(all_students)
    present_replaced = re.sub('[,]', ';', present_sample_string)
    present_replaced = re.sub("[']", '', present_replaced)
    print(course_id)
    print(absentNo, " [Absent]", excusedNo, "[Excused]", len(all_students), "[Present]")
    print(date,",",password,",",absent_replaced,",",excused_replaced,",",present_replaced)

def perfect_score():
    print("find a student with a good attendance score")

    # user = ModifiedUser.objects.get(username = "34255578")
    # print(Student.objects.get(user=user))

def query_getter():
    course = Course.objects.get(id_no = "ENGLANG 0012")
    # print(course.name)
    # print(course.lecture_set.all())
    c = course.lectures.all()
    print (course.students.all())
    # print (course.degree.all())
    d = Degree.objects.get(name = "Creative Writing (MFA)")
    # print (d.course_set.all())
    # print(c)
    for each in c:
        print(each.occurrences.filter(regular_lecture=each, is_active=False).annotate(attendance = Count("attendance_list")).aggregate(Avg('attendance')))
        

    # print (course.occurrences.all())
        # lecture_avg = LectureOccurrence.objects.filter(regular_lecture=each, is_active=False).annotate(attendance = Count("attendance_list")).aggregate(Avg('attendance'))
    # print(c.lecture_set.occurrences.all())


# randomizer()
# perfect_score()
query_getter()
# absent_excused_generator()
# actual_excuse_generator(3)


# file_reader()
if __name__ == '__main__':
    perfect_score()



# printer()

# randomizer()
