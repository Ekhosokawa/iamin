function close_session(id) {
  console.log("the close session button method worked!")
  $.ajax({
    url: '/iamin/ajax/close_session?id='+id,
    success: function(data){
      console.log(data)
      $("#"+id).empty()
      $("#"+id).append("Session closed")
      location.reload()
    }
  })
}
