// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var date_attendance;
var isStudent;
if (userType==1) {
  isStudent = true;
}
else {
  isStudent = false;
}

$('document').ready(function() {
  // var id =
  $.ajax({
    url: '/iamin/ajax/averages',
    // url: '/iamin/ajax/averages?instructor='+ins_id,
    success: function(data) {
      var all_labels = [] //what shows up on the page
      var all_names = [] //what shows up on mouseover
      var all_averages = []
      var student_avg = []
      // JsonResponse from data
      json_data = JSON.parse(data)
      courses = json_data['course']
      Object.keys(courses).forEach(key => {
        course = courses[key]
        all_labels.push(key)
        all_names.push(course['name'])
        all_averages.push(course['course_avg']);
        student_avg.push(course['student_avg'])
      })
      degrees = json_data['degree']
      Object.keys(degrees).forEach(key => {
        all_names.push(key)
        degree = degrees[key]
        all_labels.push(degree['abrv'] + "(" + degree['type'] + ")")
        all_averages.push(degree['degree_avg']);
      })
      schools = json_data['school']
      Object.keys(schools).forEach(key => {
        school = schools[key]
        all_labels.push(key)
        all_names.push(key)
        all_averages.push(school);
      })
      uni = json_data['university']
      all_labels.push('University')
      all_names.push('University')
      all_averages.push(uni);


      data = {
        labels: all_labels,
        datasets: [{
                    label: "All Students Average Attendance",
                    backgroundColor: 'rgba(0, 0, 255, 0.5)',
                    borderColor: "rgba(2,117,216,1)",
                    data: all_averages,
                    fill: false,
                    borderWidth: 1,
                    xAxisID: "bar-x-axis2"
                  },

                  {
                    label: "Student Attendance Attendance",
                    backgroundColor: 'rgba(0, 0, 0, 0.2)',
                    borderColor: "rgba(2,117,216,1)",
                    data: student_avg,
                    fill: false,
                    borderWidth: 1,
                    xAxisID: "bar-x-axis1"
                  }],
      }


      var ctx = document.getElementById("myBarChart");
      var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          scales: {
            xAxes: [
                    {
                      id: "bar-x-axis2",
                      stacked: true,
                      categoryPercentage: 0.5,
                      barPercentage: 0.5
                    },
                    {
                      display: false,
                      stacked: true,
                      id: "bar-x-axis1",
                      type: 'category',
                      categoryPercentage: 0.4,
                      barPercentage: 1,
                      gridLines: {
                          offsetGridLines: true
                      },
                      offset: true
                    }
                ],
            yAxes: [{
              ticks: {
                min: 0,
                max: 100,
                stepsize: 10
                // maxTicksLimit: 5
              },
              gridLines: {
                display: true
              }
            }],
          },
          legend: {
            display: isStudent
          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                var dataset = data.datasets[tooltipItem.datasetIndex];
                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                var currentValue = dataset.data[tooltipItem.index].toFixed(2);
                // var percentage = parseFloat((currentValue/total*100).toFixed(1));
                return currentValue + '%';
              },
              title: function(tooltipItem, data) {
                return all_names[tooltipItem[0].index];
              }
            }
          },
        }
      });
    }
  })
})
