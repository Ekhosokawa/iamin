// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var date_attendance;

$('document').ready(function() {
  // var id =
  $.ajax({
    url: '/iamin/ajax/occurrence?course='+course_slug,
    // +'&id='+id
    // not sure if above is needed
    success: function(data){
      // JsonResponse from data
      json_data = JSON.parse(data)
      date_attendance = Object.keys(json_data).map((key) => [key, json_data[key]]);
      var all_dates = []
      var all_attendance = []
      for (let i = 0; i < date_attendance.length; i++) {
        all_dates.push(date_attendance[i][0])
        all_attendance.push(date_attendance[i][1])
      }
      total = parseInt(total)
      var ctx = document.getElementById("myLineChart");
      var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels:
            all_dates,
          datasets: [{
            label: "Attendance",
            backgroundColor: "rgba(2,117,216,1)",
            borderColor: "rgba(2,117,216,1)",
            data: all_attendance,
            fill: false,
          }],
        },
        options: {
          scales: {
            xAxes: [{
              time: {
                unit: 'month'
              },
              gridLines: {
                display: false
              },
              ticks: {
                maxTicksLimit: 6
              }
            }],
            yAxes: [{
              ticks: {
                min: 0,
                max: total,
                maxTicksLimit: 5
              },
              gridLines: {
                display: true
              }
            }],
          },
          legend: {
            display: false
          },
          tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                  var currentValue = dataset.data[tooltipItem.index];
                  var percentage = parseFloat((currentValue/total*100).toFixed(1));
                  return currentValue + ' (' + percentage + '%)';
                },
                title: function(tooltipItem, data) {
                  return data.labels[tooltipItem[0].index];
                }
              }
            },
        }
      });
    }
  })
})
