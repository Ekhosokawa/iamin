
function accept(id) {
  var type = "accept";
  console.log(id)
  console.log("the accept button method worked!")
  $.ajax({
    url: '/iamin/ajax/update_absences?id='+id+'&type='+type,
    success: function(data){
      console.log(data)
      // On success, get the element acceptReject (the buttons) and empty it, then replace with "excused""
      $("#"+id).empty()
      $("#"+id).append("Excused")
    }
  })
}

function reject(id) {
  // $("") get mybuttons
  // delete mybuttons
  // add a string instead of the buttons that says rejected
  var type = "reject";
  console.log(id)
  console.log("the accept button method worked!")
  $.ajax({
    url: '/iamin/ajax/update_absences?id='+id+'&type='+type,
    success: function(data){
      console.log(data)
      // On success, get the element acceptReject (the buttons) and empty it, then replace with "absent
      $("#"+id).empty()
      $("#"+id).append("Absent(Rejected)")
    }
  })
}
