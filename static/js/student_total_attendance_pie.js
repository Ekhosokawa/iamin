// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var date_attendance;

$('document').ready(function() {
  // var id =
  $.ajax({
    url: '/iamin/ajax/student-total',
    // +'&id='+id
    // not sure if above is needed
    success: function(data) {
      // JsonResponse from data

      student_data = []
      Object.keys(data).forEach(function(key) {
        student_data.push(data[key])
      });




      var ctx = document.getElementById("myPieChart");

      var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: ["Present", "Absent", "Excused", "Pending"],
          datasets: [{
            data: student_data,
            backgroundColor: ['#28a745', '#dc3545', '#ffc107', '#007bff'],
          }],
        },
        options: {
          tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                  var total = meta.total;
                  var currentValue = dataset.data[tooltipItem.index];
                  var percentage = parseFloat((currentValue/total*100).toFixed(1));
                  return currentValue + ' (' + percentage + '%)';
                },
                title: function(tooltipItem, data) {
                  return data.labels[tooltipItem[0].index];
                }
              }
            },
        }
      });
    }
  })
})
