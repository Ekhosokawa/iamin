$('document').ready(function() {
  send_money_request()
})


function send_money_request() {
  $('#table_body').empty();
  let cost = $("#uniprice").val()
  $.ajax({
    url: '/iamin/ajax/money-waste?cost='+cost,
    success: function(data){
      jQuery.each(data, function(i, val) {
        var row = $('<tr>')
        row.append($('<td>').text(i))
        jQuery.each(val, function(key, value) {
          row.append($('<td>').text(value.toFixed(2)));
          console.log(value);
        });
        $('#table_body').append(row);
      });
    }
  })
}
