// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var ctx = document.getElementById("myPieChart");
var unaccounted = total - present - absent - excused - pending;

// // The following if-statement is hard-coded for evaluation purposes
// if (occurrence_name == "Modern Jewish Literature" && occurrence_date == "Feb. 12, 2020, 9 a.m." &&
//   occurrence_password != "None" &&  occurrence_is_active) {
//   unaccounted = 0
//   present = 2
//   excused = 2
// }


var myPieChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: ["Present", "Absent", "Excused", "Pending" , "Unaccounted"],
    datasets: [{
      data: [present, absent, excused, pending, unaccounted],
      backgroundColor: ['#28a745', '#dc3545', '#ffc107', '#007bff', '#979999'],
    }],
  },
  options: {
    tooltips: {
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
            var total = meta.total;
            var currentValue = dataset.data[tooltipItem.index];
            var percentage = parseFloat((currentValue/total*100).toFixed(1));
            return currentValue + ' (' + percentage + '%)';
          },
          title: function(tooltipItem, data) {
            return data.labels[tooltipItem[0].index];
          }
        }
      },
  }
});
