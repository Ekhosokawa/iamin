from django.conf.urls import url
from iamin import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^help/$', views.help, name='help'),
    url(r'^clear-database/$', views.clear_database, name='clear_database'),
    url(r'^forgot-password/$', views.forgot_password, name='forgot_password'),
    url(r'^courses/$', views.show_courses, name='show_courses'),
    url(r'^student/statistics/$', views.student_statistics, name='student_statistics'),
    url(r'^student/all-notifications/$', views.student_notifications_all, name='student_notifications_all'),
    url(r'^instructor/statistics/$', views.instructor_statistics, name='instructor_statistics'),
    url(r'^register/$', views.register, name='register'),
    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/$',
        views.show_lectures, name ='show_lectures'),

    url(r'^ajax/close_session$', views.close_session, name='close_session'),
    url(r'^ajax/update_absences$', views.update_absences, name='update_absences'),
    url(r'^ajax/occurrence$', views.create_date_attendance_chart, name='create_date_attendance_chart'),
    url(r'^ajax/averages$', views.create_averages_chart, name='create_averages_chart'),
    url(r'^ajax/student-total$', views.ajax_averages_pie, name='create_student_total_chart'),
    url(r'^ajax/money-waste$', views.ajax_money_waste, name='money_waste'),
    # url(r'^ajax/line-chart$', views.ajax_line_chat_attendance, name='create_student_total_chart'),
    # url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/instructor/absences',
    #         views.instructor_absences, name ='instructor_absences'),

    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/student/absences',
        views.student_absences, name ='student_absences'),
    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/instructor/absences',
        views.instructor_absences, name ='instructor_absences'),

    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/instructor/notify',
        views.instructor_notify, name='instructor_notify'),
    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/student/notifications',
        views.student_notifications, name='student_notifications'),

    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/instructor',
        views.instructor_attendance, name ='instructor_attendance'),
    url(r'^courses/(?P<course_id_no_slug>[\w\-]+)/student',
        views.student_attendance, name ='student_attendance'),
]
