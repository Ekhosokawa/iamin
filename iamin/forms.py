from django import forms
from django.contrib.auth.models import AbstractUser
from iamin.models import ModifiedUser, Student, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence

class ModifiedUserForm(forms.ModelForm):
    username = forms.RegexField(required=True, regex=r'^[0-9]+$', label='studentID', max_length=8,
                    min_length=8, widget=forms.TextInput(attrs={'placeholder': "12345678"}), error_messages={'error': 'Please enter your StudentID'})

    class Meta:
        model = ModifiedUser
        # error_messages={'required': 'Your username must be your 8-digit student id.'},
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

class StudentForm(forms.ModelForm):
    accept = forms.BooleanField(required=True)
    class Meta:
        model = Student
        fields = ('accept', 'degree')

class CreatePasswordForm(forms.ModelForm):
    date = forms.DateTimeField(widget=forms.HiddenInput())
    instructor = forms.ModelChoiceField(queryset=Instructor.objects.all(), widget=forms.HiddenInput())
    regular_lecture = forms.ModelChoiceField(queryset=Lecture.objects.all(), widget=forms.HiddenInput())
    session_password = forms.CharField(required=True, min_length=3)
    class Meta:
        model = LectureOccurrence
        fields = ('date', 'session_password', 'instructor', 'regular_lecture')

class SubmitPasswordForm(forms.ModelForm):
    lecture_occurrence = forms.ModelChoiceField(queryset=LectureOccurrence.objects.all(), widget=forms.HiddenInput())
    student = forms.ModelChoiceField(queryset=Student.objects.all(), widget=forms.HiddenInput())
    session_password = forms.CharField(required=True, min_length=3)
    class Meta:
        model = StudentLectureOccurrence
        fields = ('session_password', 'lecture_occurrence', 'student',)

class SubmitExcuseForm(forms.ModelForm):
    lecture_occurrence = forms.ModelChoiceField(queryset=LectureOccurrence.objects.all(), widget=forms.HiddenInput())
    student = forms.ModelChoiceField(queryset=Student.objects.all(), widget=forms.HiddenInput())
    excuse = forms.CharField(required=True, min_length=3)
    class Meta:
        model = StudentLectureOccurrence
        fields = ('excuse', 'lecture_occurrence', 'student',)

class NotificationForm(forms.ModelForm):
    type_choices = [
        ("cancel", "Lecture canceled"),
        ("other," "Other")
        ]
    date = forms.DateTimeField(disabled = True, )
    instructor = forms.ModelChoiceField(disabled = True, queryset=Instructor.objects.all())
    regular_lecture = forms.ModelChoiceField(disabled = True, queryset=Lecture.objects.all())
    was_canceled = forms.BooleanField(required=False, label="Lecture cancellation")

    #, widget=forms.HiddenInput()
    # type = forms.CharField(required=True, label='Type', widget=forms.Select(choices=type_choices))
    message = forms.Textarea()
    class Meta:
        model = LectureOccurrence
        fields = ('was_canceled', 'regular_lecture', 'date', 'instructor', 'message',)

# class SubmitExcuseForm(forms.ModelForm):
#     lecture_occurrence = forms.ModelChoiceField(queryset=LectureOccurrence.objects.all())
#     student = forms.ModelChoiceField(queryset=Student.objects.all())
#     excuse = forms.CharField(required=True, min_length=3)
#     class Meta:
#         model = StudentLectureOccurrence
#         fields = ('excuse', 'lecture_occurrence', 'student',)
