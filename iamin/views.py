from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from iamin.models import Degree, Student, Instructor, Course, Lecture, LectureOccurrence, StudentLectureOccurrence
from iamin.forms import ModifiedUserForm, StudentForm, CreatePasswordForm, SubmitPasswordForm, SubmitExcuseForm, NotificationForm
from django.contrib.auth.decorators import login_required
from datetime import datetime
from django.db.models import Count, Avg
import json
import pytz
from update import *
# from test.py import perfect_score



#Python conventions for methods use underscore (snake_case) over camelCase.

def course_dict(user):
    #Returns an array of the user's courses. Used across multiple view methods
    course_list = []
    if user.user_type==1:
        course_list = Course.objects.filter(
        students = user.student
        )
    elif user.user_type==2:
        course_list = Course.objects.filter(
            instructor = user.instructor
        )
    return (course_list)

@login_required
def home(request):
    user = request.user
    course_list = course_dict(user)
    context_dict = {"courses": course_list,}
    if (user.user_type==1):
        return render(request, 'iamin/student/home.html', context=context_dict)
    else:
        return render(request, 'iamin/instructor/home.html', context=context_dict)

def help(request):
    user = request.user
    course_list = course_dict(user)
    context_dict = {"courses": course_list,}
    return render(request, 'iamin/help.html', context=context_dict)

def forgot_password(request):
    context_dict = {}
    return render(request, 'iamin/forgot_password.html', context=context_dict)

def clear_database(request):
    #Clears all data for any updated population stuff
    user = request.user
    course_list = course_dict(user)
    #call method from update.py file, deleting the modified LectureOccurrences
    clear_current("ENGLANG 0014", "Tue", "2020-02-11")
    clear_current("COMPLIT 1392", "Wed", "2020-02-12")
    clear_current("ENGLANG 0014", "Tue", "2020-02-18")
    clear_current("ENGLANG 0014", "Tue", "2020-02-25")
    #rerun update method, recreating the deleted LecOccs w/ original data
    update_current()
    print (user.user_type)
    #If user is a student, also clear
    if (user.user_type==1):
        clear_SLO(user)
        print ("hey, delete all the student occurrences!")
        update_current()


    context_dict = {"courses": course_list,}
    return render(request, 'iamin/cleared.html', context=context_dict)

@login_required
def show_courses(request):
    user = request.user
    course_list = course_dict(user)
    instructor_dict={}
    degree_dict = {}
    for course in course_list:
        ins_query = course.instructor.all()
        instructor_list = query_parser(ins_query)
        instructor_dict[course] = instructor_list
        deg_query = course.degree.all()
        degree_list = query_parser(deg_query)
        degree_dict[course]=degree_list
    context_dict = {'instructor_dict': instructor_dict, "courses": course_list, 'degree_dict' : degree_dict}
    return render(request, 'iamin/courses.html', context=context_dict)

@login_required
def show_lectures(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)
    course = Course.objects.get(slug = course_id_no_slug)
    occurrence_list=[]
    lecture_query = Lecture.objects.filter(course = course)
    for lecture in lecture_query:
        occurrence_query = LectureOccurrence.objects.filter(regular_lecture = lecture)
        occurrence_list.extend(query_parser(occurrence_query))
    context_dict = {"course" : course, "lectures": lecture_query, "occurrences": occurrence_list, "courses": course_list,}
    return render(request, 'iamin/lectures.html', context=context_dict)

@login_required
def instructor_attendance(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)
    course = Course.objects.get(slug = course_id_no_slug)
    lecture_query = Lecture.objects.filter(course = course)
    occ_id = request.GET.get('occ_id',-1)
    occurrence = LectureOccurrence.objects.get(id=occ_id)
    total = course.students.count()
    create_password_form=CreatePasswordForm(request.POST or None, instance=occurrence)
    if request.method == 'POST':
        if create_password_form.is_valid():
            password = create_password_form.save()
            # For evalation purposes
            if (course_id_no_slug == "complit-1392"):
                eval_students = ["34623456", "15834571"]
                for eval in eval_students:
                    user = ModifiedUser.objects.get(username=eval)
                    student = Student.objects.get(user=user)
                    slo = StudentLectureOccurrence.objects.get_or_create(
                    lecture_occurrence=occurrence, student=student)[0]
                    slo.attended=1
                    occurrence.attendance_list.add(slo.student)
                    occurrence.save()
                    slo.save()
            # print (password.session_password)
        else:
            print(create_password_form.errors)
    context_dict = {"occurrence": occurrence, "course" : course,
        "create_password_form" : create_password_form, "total": total, "courses": course_list,
        }
    return render(request, 'iamin/instructor/attendance.html', context=context_dict)

@login_required
def student_attendance(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)
    course = Course.objects.get(slug = course_id_no_slug)
    all_course_students = course.students.all()
    occ_id = request.GET.get('occ_id',-1)
    #isCourseStudent is a flag to check if the student is enrolled.
    isCourseStudent=False
    #Checks to see if the the student is enrolled in the course (compares stu object
    #from all_course_students array to the student attribute in StudentLectureOccurrence)
    for stu in all_course_students:
        if (stu.user==user):
            isCourseStudent=True
    #If the student isn't part of the course, they're redirected to the homepage.
    #Otherwise, they can view the submit password form below.
    if (not isCourseStudent):
        # print ("you aren't part of this course")
        # print ("redirecting to homepage")
        return redirect('home')
    #Here, we set up the LectureOccurrence object, as well as the specific
    #StudentLectureOccurrence object.
    occurrence = LectureOccurrence.objects.get(id=occ_id)
    student=Student.objects.get(user=user)
    slo = StudentLectureOccurrence.objects.get_or_create(
        lecture_occurrence=occurrence, student=student)[0]
    sp_form=SubmitPasswordForm(request.POST or None, instance = slo)
    #Here, we check to see if the submitted form is correct.
    if request.method == 'POST':
        if sp_form.is_valid():
            #save the SubmitPasswordForm but don't commit it to the database yet
            spf = sp_form.save(commit=False)
            #check to see if the submitted password matches the instructor's password
            if spf.session_password == occurrence.session_password:
                #if passwords match, the student's "attended" becomes 1 (which means attended),
                #and they are added to the Lecture's array of attending students.
                spf.attended=1
                #also need to update the occurrence's "attendance_list" object for use in statistics
                occurrence.attendance_list.add(slo.student)
                # print (occurrence.attendance_list.all())
                spf.save()
            else:
                print("wrong password")
    else:
        # print("looks like the sp_form is invalid")
        print(sp_form.errors)
    context_dict = {"occurrence": occurrence, "slo": slo, "course" : course, "sp_form" : sp_form, "courses": course_list,}
    return render(request, 'iamin/student/attendance.html', context=context_dict)

@login_required
def student_absences(request, course_id_no_slug):
    #reuses the same logic as student_attendance
    user = request.user
    course_list = course_dict(user)
    course = Course.objects.get(slug = course_id_no_slug)
    all_course_students = course.students.all()
    occ_id = request.GET.get('occ_id',-1)
    isCourseStudent=False
    for stu in all_course_students:
        if (stu.user==user):
            isCourseStudent=True
    if (not isCourseStudent):
        # print ("you aren't part of this course")
        # print ("redirecting to homepage")
        return redirect('home')
    occurrence = LectureOccurrence.objects.get(id=occ_id)
    student=Student.objects.get(user=user)
    slo = StudentLectureOccurrence.objects.get_or_create(
        lecture_occurrence=occurrence, student=student)[0]
    se_form=SubmitExcuseForm(request.POST or None, instance = slo)
    if request.method == 'POST':
        if se_form.is_valid():
            sef = se_form.save()
            occurrence.pending_list.add(student)
            slo.attended=3
            slo.save()
            occurrence.save()
    else:
        # print("looks like the se_form is invalid")
        print(se_form.errors)
    context_dict = {"occurrence": occurrence, "slo": slo, "course" : course, "se_form" : se_form, "courses": course_list,}

    return render(request, 'iamin/student/absences.html', context=context_dict)

@login_required
def instructor_absences(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)
    course = Course.objects.get(slug = course_id_no_slug)
    all_course_students = course.students.all()
    occ_id = request.GET.get('occ_id',-1)
    occurrence = LectureOccurrence.objects.get(id=occ_id)
    absent_list=[]
    excused_list=[]
    pending_list=[]
    slo_list = StudentLectureOccurrence.objects.filter(
        lecture_occurrence=occurrence)
    for slo in slo_list:
        if (slo.attended == 3):
            pending_list.append(slo)
            student = slo.student
            occurrence.pending_list.add(student)
            occurrence.save()
        elif (slo.attended == 2):
            excused_list.append(slo)
            student = slo.student
            occurrence.excused_list.add(student)
            occurrence.save()
        elif (slo.attended == 0):
            absent_list.append(slo)
            student = slo.student
            occurrence.absent_list.add(student)
            occurrence.save()
    context_dict = {"occurrence": occurrence, "course" : course, "courses": course_list, 'pending': pending_list, 'excused': excused_list, 'absent': absent_list}
    return render(request, 'iamin/instructor/absences.html', context=context_dict)

@login_required
def student_notifications(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)

    course = Course.objects.get(slug = course_id_no_slug)
    all_course_students = course.students.all()
    occ_id = request.GET.get('occ_id',-1)
    occurrence = LectureOccurrence.objects.get(id=occ_id)

    notify_form=NotificationForm(request.POST or None, instance=occurrence)
    if request.method == 'POST':
        if notify_form.is_valid():
            notify_form.save()
            if (occurrence.was_canceled):
                occurrence.is_active = False
                occurrence.save()

        else:
            print(notify_form.errors)

    context_dict = {"courses": course_list, "course": course, "occurrence": occurrence, "notify_form": notify_form}
    return render(request, 'iamin/student/notifications.html', context=context_dict)

@login_required
def student_notifications_all(request):
    user = request.user
    course_list = course_dict(user)
    notification_list = []
    for course in course_list:
        reg_query = Lecture.objects.filter(course = course)
        reg_lectures=query_parser(reg_query)
        # print (course)
        for each in reg_lectures:
            lecture_query = LectureOccurrence.objects.filter(regular_lecture=each).exclude(message=None)
            if lecture_query:
                notification_list += query_parser(lecture_query)

    context_dict = {"courses": course_list, "notifications": notification_list}
    return render(request, 'iamin/student/notifications_all.html', context=context_dict)

@login_required
def instructor_notify(request, course_id_no_slug):
    user = request.user
    course_list = course_dict(user)

    course = Course.objects.get(slug = course_id_no_slug)
    all_course_students = course.students.all()
    occ_id = request.GET.get('occ_id',-1)
    occurrence = LectureOccurrence.objects.get(id=occ_id)


    notify_form=NotificationForm(request.POST or None, instance=occurrence)
    if request.method == 'POST':
        if notify_form.is_valid():
            notify_form.save()
            if (occurrence.was_canceled):
                occurrence.is_active = False
                occurrence.save()

        else:
            print(notify_form.errors)

    context_dict = {"courses": course_list, "course": course, "occurrence": occurrence, "notify_form": notify_form}
    return render(request, 'iamin/instructor/notify.html', context=context_dict)


@login_required
def student_statistics(request):
    user = request.user
    course_list = course_dict(user)
    context_dict = {"courses": course_list,}
    return render(request, 'iamin/student/statistics.html', context=context_dict)

@login_required
def instructor_statistics(request):
    user = request.user
    course_list = course_dict(user)

    context_dict = {'courses': course_list}
    return render(request, 'iamin/instructor/statistics.html', context=context_dict)

def course_attendance_average(Course):
    #finds average attendance as percentage
    all_lectures = []
    att_count = 0
    avg = 0
    total = Course.students.count()
    #Find all lectures from course
    reg_query = Lecture.objects.filter(course = Course)
    reg_lectures=query_parser(reg_query)
    #find all the closed lecture occurrences of the course
    course_avg = 0
    for each in reg_lectures:
        #Average for lecture occurrence attendance, per type of lecture
        lecture_avg = LectureOccurrence.objects.filter(regular_lecture=each, is_active=False, was_canceled=False).annotate(attendance = Count("attendance_list")).aggregate(Avg('attendance'))
        course_avg += lecture_avg['attendance__avg']
    course_avg = course_avg/len(reg_lectures)
    #clearing out all lectures with 0 closed session
    if (course_avg != 0):
        avg = course_avg/total
    percent_avg = 100 * avg
    return (percent_avg)

def degree_attendance_average(Degree):
    # avg_total_per_course = Course.objects.filter(degree = Degree).annotate(num_students = Count("students")).aggregate(Avg('num_students'))
    # print(avg_total_per_course)
    course_query = Degree.course_set.all()
    all_courses = query_parser(course_query)
    sum = 0
    avg = 0
    counter = 0
    for each in all_courses:
        sum += course_attendance_average(each)
        if (course_attendance_average(each) != 0):
            counter+=1
    #Remove all courses that have an average attendance of 0

    avg = sum/counter
    return (avg)

def school_attendance_average(school):
    degree_query = Degree.objects.filter(school = school)
    all_degrees = query_parser(degree_query)
    sum = 0
    avg = 0
    counter = 0
    all_courses = []
    for each in all_degrees:
        course_query = each.course_set.all()
        all_courses = query_parser(course_query)
        for c in all_courses:
            sum += course_attendance_average(c)
            if (course_attendance_average(c) != 0):
                counter+=1


        # sum += degree_attendance_average(each)
        # if (degree_attendance_average(each) == 0):
        #     counter+=1
    # Remove all courses that have an average attendance of 0

    avg = sum/counter
    print ("school attendance average:", avg)
    return (avg)

def university_attendance_average():
    course_query = Course.objects.all()
    all_courses = query_parser(course_query)
    sum = 0
    avg = 0
    counter = 0
    for each in all_courses:
        sum += course_attendance_average(each)
        if (course_attendance_average(each) == 0):
            counter+=1
    true_total=len(all_courses)-counter
    if (true_total > 0):
        avg = sum/true_total
    return (avg)

def query_parser(array):
    list=[]
    for each in array:
        list.append(each)
    return(list)

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = ModifiedUserForm(data=request.POST)
        student_form = StudentForm(data=request.POST)
        if user_form.is_valid() and student_form.is_valid():
            #Here, we save the student_form information as "student"
            #without committing to the database.
            student = student_form.save(commit = False)
            user = user_form.save()
            #Sets the password and user type to 1 (student) This is by design.
            #Instructors must be added to the database by an admin.
            user.set_password(user.password)
            user.user_type=1
            student.user = user
            user.save()
            student.save()
            registered = True
        else:
            print(user_form.errors)

    else:
        user_form=ModifiedUserForm()
        student_form = StudentForm()
    return render (request, 'iamin/register.html',
            {'user_form': user_form, 'student_form': student_form, 'registered': registered})


#Respond to an AJAX request; allows instructor to accept/reject absences
@login_required
def update_absences(request):
    if request.method == 'GET':
        query = request.GET
        print("in update aabsences view")
        dictQuery = dict(query)
        id = dictQuery['id'][0];
        type = dictQuery['type'][0];
        print(id)
        print(type)
        slo = StudentLectureOccurrence.objects.get(id=id)
        occ = LectureOccurrence.objects.get(id=slo.lecture_occurrence.id)
        print(occ)
        print (slo)
        print (slo.student.user.first_name, slo.student.user.last_name, slo.excuse)
        if (type=="accept"):
            slo.attended = 2
            print(slo.student)
            occ.excused_list.add(slo.student)
            occ.absent_list.remove(slo.student)
            occ.pending_list.remove(slo.student)
            slo.save()
            occ.save()
        if (type=="reject"):
            slo.attended = 0
            print(slo.student)
            occ.absent_list.add(slo.student)
            occ.excused_list.remove(slo.student)
            occ.pending_list.remove(slo.student)
            slo.save()
            occ.save()
        print(occ.excused_list.all())
        print(occ.absent_list.all())
        print(occ.pending_list.all())
    return JsonResponse({'success' : True})

@login_required
def close_session(request):
    if request.method == 'GET':
        query = request.GET
        print("in close session view")
        dictQuery = dict(query)
        id = dictQuery['id'][0];
        print(id)
        occ = LectureOccurrence.objects.get(id=id)
        occ.is_active = False
        unaccounted = []
        course = Course.objects.get(id=occ.regular_lecture.course.id)
        #add all students to the unaccounted list
        for s in course.students.all():
            unaccounted.append(s)
        #remove all attending/excused/pending students from list
        for s in occ.attendance_list.all():
            unaccounted.remove(s)
        for s in occ.excused_list.all():
            unaccounted.remove(s)
        for s in occ.pending_list.all():
            unaccounted.remove(s)
        #add all unaccounted students to absent list of LectureOccurrence
        #StudentLectureOccurrence does not need to be updated, as default is 0 on object creation (absent)
        for s in unaccounted:
            occ.absent_list.add(s)
        occ.save()
    return JsonResponse({'success' : True})

@login_required
def create_date_attendance_chart(request):
    if request.method == 'GET':
        print ("in create date attendance chart view")
        query = request.GET
        dictQuery = dict(query)
        course_id_no_slug = dictQuery['course'][0];
        course = Course.objects.get(slug = course_id_no_slug)
        lecture_list = Lecture.objects.filter(course = course)
        occurrence_list = []
        date_attend={}
        for lecture in lecture_list:
            occurrence_query = lecture.occurrences.all()
            print (occurrence_query)
            occurrence_list.extend(query_parser(occurrence_query))
        #makes two arrays, one datetime as string, one the total attendance count, and associates them
        for o in occurrence_list:
            datestring = o.date.strftime('%Y-%m-%d %H:%M')
            d = datestring
            a = o.attendance_list.count()
            date_attend[d] = a
        #sorting date_attend
        date_attend_dict = {}
        for key in sorted(date_attend):
            date_attend_dict[key]=date_attend[key]
    date_attend_dict_obj = json.dumps(dict(date_attend_dict))
    return JsonResponse(date_attend_dict_obj, safe= False)

@login_required
def create_averages_chart(request):
    user=request.user
    course_list = course_dict(user)
    # print( course_list)
    course_data = {}
    degree_list=set()
    degree_data = {}
    school_list = set()
    school_data = {}
    #Maybe add type average (MSc, BSc, etc.) (population script doesn't include more than one degree per type)
    uni_data={}
    #Old code: made to be more dynamic, kept just in case
    # for each in course_list:
    #     #using ??? as a string split marker--unlikely to show up in any course name
    #     #passes both id_no (displayed on load) and name (displayed on hover)
    #     course_data[each.id_no + "???" + each.name]=course_attendance_average(each)
    #     degree_query=each.degree.all()
    #     degree_list.update(degree_query)
    # for each in degree_list:
    #     degree_data[each.abrv + "(" + each.type + ")???" + each.name]=degree_attendance_average(each)

    for each in course_list:
        if (user.user_type == 1) :
            course_data[each.id_no]= {'name' : each.name, 'course_avg': course_attendance_average(each), 'student_avg' : student_attendance_course(each, user)}
        else:
            course_data[each.id_no]= {'name' : each.name, 'course_avg': course_attendance_average(each)}
        degree_query=each.degree.all()
        degree_list.update(degree_query)

    if (user.user_type ==2) :
        for each in degree_list:
            degree_data[each.name] = {'abrv' : each.abrv, 'type' : each.type, 'degree_avg' : degree_attendance_average(each)}
            school = each.school
            school_list.add(school)
    else:
        degree = user.student.degree
        degree_data[degree.name] = {'abrv' : degree.abrv, 'type' : degree.type, 'degree_avg' : degree_attendance_average(degree)}

    for each in school_list:
        school_data[school]=school_attendance_average(each)

    uni_avg=university_attendance_average()

    all_data ={}
    all_data['course'] = course_data
    all_data['degree'] = degree_data
    all_data['school'] = school_data
    all_data['university'] = uni_avg

    all_data_obj = json.dumps(dict(all_data))

    return JsonResponse(all_data_obj, safe= False)


def student_attendance_course(course, user):
    student=Student.objects.get(user=user)
    number_lectures = 0
    number_attendance = 0

    for lecture in course.lectures.all() :
        number_lectures += lecture.occurrences.all().filter(is_active=False, was_canceled=False).count()
        number_attendance += lecture.occurrences.all().filter(was_canceled=False, attendance_list__id__exact = student.id).count()
    return (number_attendance/number_lectures  * 100)

@login_required
def ajax_averages_pie(request):
    student = request.user.student
    number_attendance = 0
    number_absent = 0
    number_excused = 0
    number_pending = 0


    for course in student.degree.course_set.all() :
        for lecture in course.lectures.all() :
            number_attendance += lecture.occurrences.all().filter(was_canceled=False, attendance_list__id__exact = student.id).count()
            number_absent += lecture.occurrences.all().filter(was_canceled=False, absent_list__id__exact = student.id).count()
            number_excused += lecture.occurrences.all().filter(was_canceled=False, excused_list__id__exact = student.id).count()
            number_pending += lecture.occurrences.all().filter(was_canceled=False, pending_list__id__exact = student.id).count()

    data = {"attendance": number_attendance, "absent": number_absent, "excused": number_excused, "pending": number_pending}


    # all_data_obj = json.dumps(dict(all_data))

    return JsonResponse(data, safe= False)

# @login_required
# def ajax_line_chat_attendance(request):
#     student = request.user.student
#     number_attendance = 0
#     number_absent = 0
#     number_excused = 0
#     number_pending = 0
#     # obtain ordered list of dates
#     for course in student.degree.course_set.all() :
#         for lecture in course.lectures.all() :
#             print(lecture.occurrences.all().filter(was_canceled=False, attendance_list__id__exact = student.id, ).order_by('date').distinct('date'))
#     data = {"attendance": "ciao"}
#     # all_data_obj = json.dumps(dict(all_data))
#
#     return JsonResponse(data, safe= False)

@login_required
def create_student_total_chart(request):
    user=request.user
    course_list = course_dict(user)




    if request.method == 'GET':
        print ("in create averages chart view")
    all_data_obj = json.dumps(dict(all_data))
    next_main = datetime.now()
    delta = next_main-prev_main
    print("ALL STATS")
    print(delta)
    return JsonResponse(all_data_obj, safe= False)

@login_required
def ajax_money_waste(request):
    query = request.GET
    dictQuery = dict(query)
    cost = dictQuery['cost'][0];
    student = request.user.student
    context_dict = {}
    # obtain ordered list of dates
    total_lecture_occurrences = 0
    total_not_there = 0
    for course in student.degree.course_set.all() :
        course_lectures_occurrences = 0
        course_not_there = 0
        for lecture in course.lectures.all() :
            course_lectures_occurrences += lecture.occurrences.all().count()
            total_lecture_occurrences += lecture.occurrences.all().count()
            course_not_there += lecture.occurrences.all().filter(was_canceled=False, excused_list__id__exact = student.id, ).count()
            course_not_there += lecture.occurrences.all().filter(was_canceled=False, absent_list__id__exact = student.id, ).count()
            total_not_there += course_not_there

        context_dict[course.name] = {"total" : course_lectures_occurrences, "skipped" : course_not_there}

    lesson_price = int(cost) / total_lecture_occurrences
    for k, v in context_dict.items():
        v["skipped"]
        context_dict[k]["money_lost"] = v["skipped"] * lesson_price

    context_dict["Total Count"] = {"total" : total_lecture_occurrences, "skipped" : total_not_there, "money_lost" : (total_not_there * lesson_price)}

    return JsonResponse(context_dict, safe= False)
