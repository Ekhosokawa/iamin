from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import AbstractUser
import json

#Python conventions for methods use underscore (snake_case) over camelCase.
class ModifiedUser (AbstractUser):
    #Here, we use Django's AbstractUser to create user types.
    #CHOICE is in allcaps because it is a final.
    CHOICE = [
        (0, 'nonUser'),
      (1, 'student'),
      (2, 'instructor'),
      # (3, 'school admin'),
      # (4, 'database admin'),
    ]
    user_type = models.PositiveSmallIntegerField(choices=CHOICE, default=0)


class Degree(models.Model):
    name = models.CharField(max_length=128, unique=True)
    type = models.CharField(max_length=8)
    school = models.CharField(max_length=128)
    abrv=models.CharField(max_length=10)
    def __str__(self):
        return self.name

class Student (models.Model):
    user = models.OneToOneField(ModifiedUser)
    # id_no = models.CharField(max_length=8, unique=True)
    degree = models.ForeignKey(Degree, related_name = "degree")
    accept = models.BooleanField(default = False)

    def __str__(self):
        return '%s (%s %s)' % (self.user.username, self.user.first_name, self.user.last_name)
    # def save(self, *args, **kwargs):
    #     self.user.user_type = 1
    #     super(Student, self).save(*args, **kwargs)

class Instructor (models.Model):
    user = models.OneToOneField(ModifiedUser)
    # id_no = models.CharField(max_length=8, unique=True)
    def __str__(self):
        return '%s (%s %s)' % (self.user.username, self.user.first_name, self.user.last_name)

class Course (models.Model):
    name = models.CharField(max_length=128)
    id_no = models.CharField(max_length=15, unique=True)
    instructor = models.ManyToManyField(Instructor)
    degree = models.ManyToManyField(Degree)
    students = models.ManyToManyField(Student)
    slug = models.SlugField(unique=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.id_no)
        super(Course, self).save(*args, **kwargs)
    def __str__(self):
        return '%s (%s)' % (self.id_no, self.name)
        #make slug

class Lecture (models.Model):
    course = models.ForeignKey(Course, related_name = "lectures")
    day = models.CharField(max_length=3, null=True)
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)
    location = models.CharField(max_length=128, null=True)
    def __str__(self):
        return '%s (%s %s)' % (self.course.name, self.day, self.start_time)

class LectureOccurrence (models.Model):
    regular_lecture = models.ForeignKey(Lecture, related_name = "occurrences")
    date = models.DateTimeField()
    instructor = models.ForeignKey(Instructor)
    attendance_list = models.ManyToManyField(Student, related_name="attendance", blank =True)
    absent_list = models.ManyToManyField(Student, related_name="absent", blank= True)
    excused_list= models.ManyToManyField(Student, related_name="excused", blank = True)
    pending_list= models.ManyToManyField(Student, related_name="pending", blank = True)
    session_password = models.CharField(max_length=30, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    was_canceled = models.BooleanField(default=False, )
    message = models.TextField(max_length = 250, null=True, blank=True)
    class Meta:
        unique_together = (("regular_lecture", "date"),)
    def __str__(self):
        return '%s (%s)' % (self.regular_lecture.course.name, self.date)

class StudentLectureOccurrence (models.Model):
    lecture_occurrence = models.ForeignKey(LectureOccurrence)
    student = models.ForeignKey(Student)
    session_password = models.CharField(max_length=30, null=True)
    excuse = models.CharField(max_length=156, null=True)
    CHOICE = [
        (0, 'absent'),
      (1, 'present'),
      (2, 'excused'),
      (3, 'pending'),
    ]
    attended = models.PositiveSmallIntegerField(choices=CHOICE, default=0)
    class Meta:
        unique_together = (("lecture_occurrence", "student"))
    def __str__(self):
        return '%s (%s %s)' % (self.student.user.username,
            self.lecture_occurrence.date,
            self.lecture_occurrence.regular_lecture.course.id_no)
