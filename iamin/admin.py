from django.contrib import admin
from iamin.models import Degree, Student, Course, Instructor, Lecture, LectureOccurrence, StudentLectureOccurrence, ModifiedUser


# class LectureAdmin(admin.ModelAdmin):
#     list_display =
#
# class PageAdmin(admin.ModelAdmin):
#     list_display = ('title', 'category', 'url', 'views')
#

admin.site.register(Degree)
admin.site.register(Student)
admin.site.register(Course)
admin.site.register(Instructor)
admin.site.register(Lecture)
admin.site.register(LectureOccurrence)
admin.site.register(ModifiedUser)
admin.site.register(StudentLectureOccurrence)

#update this later--try Lecture, LectureOccurrence
